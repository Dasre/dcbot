const Discord = require('discord.js');
const axios = require('axios');
const { DateTime } = require('luxon');
const auth = require('./auth.json');

const bot = new Discord.Client();
bot.on('ready', () => {
  console.log('Ready GO!');
});
bot.login(auth.token);

// 秒數轉時:分:秒
const secondFormat = (second) => {
  let secondTime = parseInt(second, 10);
  let minuteTime = 0;
  let hourTime = 0;

  if (secondTime > 60) {
    minuteTime = parseInt(secondTime / 60, 10);
    secondTime = parseInt(secondTime % 60, 10);
    if (minuteTime > 60) {
      hourTime = parseInt(minuteTime / 60, 10);
      minuteTime = parseInt(minuteTime % 60, 10);
    }
  }
  const result = `還有${hourTime}小時${minuteTime}分鐘${secondTime}秒`;
  return result;
};

bot.on('message', async (message) => {
  if (message.content.substring(0, 1) === '!') {
    const args = message.content.substring(1).split(' ');

    // 關鍵字
    const cmd = args[0];

    switch (cmd) {
      case 'sc': { // 填刀
      // 第幾刀
        const num = args[1];
        // 出刀分數
        const score = args[2];

        let scoreOne = 0;
        let scoreTwo = 0;
        if (score.indexOf('+') !== -1) {
          const scoresplice = score.split('+');
          [scoreOne, scoreTwo] = [scoresplice[0], scoresplice[1]];
        } else {
          scoreOne = score;
        }

        // 幾階段王
        const bossNumber = args[3];
        let boss;
        if (bossNumber === '1') {
          boss = 'blue';
        } else if (bossNumber === '2') {
          boss = 'brown';
        } else {
          boss = 'black';
        }

        // 傳到後端 Apps Script的資料
        const config = JSON.stringify({
          num,
          date: '23',
          name: 'coe',
          one: scoreOne,
          one_other: scoreTwo,
          boss_one: boss,
        });

        // 填入Google Sheets 部屬後的URL
        // URL後面請放GET參數 q=score&num=${num}&date=23&name=coe
        // 須根據Apps Script的回傳做調整
        await axios.get(URL)
          .then((res) => {
            const one = res.data.one[0][0] !== undefined ? res.data.one[0][0] : '0';
            const two = res.data.two[0][0] !== undefined ? res.data.two[0][0] : '0';

            switch (num) {
              case '1': {
                const embed = new Discord.RichEmbed()
                  .setColor('#0099ff')
                  .setTitle('今日出刀紀錄')
                  .setAuthor(message.author.username)
                  .addField('第一刀', scoreOne, true)
                  .addField('第一刀補償刀', scoreTwo, true)
                  .addField('第二刀', one, true)
                  .addField('第三刀', two, true)
                  .setTimestamp();
                message.channel.send(embed);
                break;
              }
              case '2': {
                const embed = new Discord.RichEmbed()
                  .setColor('#0099ff')
                  .setTitle('今日出刀紀錄')
                  .setAuthor(message.author.username)
                  .addField('第一刀', one, true)
                  .addField('第二刀', scoreOne, true)
                  .addField('第二刀補償刀', scoreTwo, true)
                  .addField('第三刀', two, true)
                  .setTimestamp();
                message.channel.send(embed);
                break;
              }
              case '3': {
                const embed = new Discord.RichEmbed()
                  .setColor('#0099ff')
                  .setTitle('今日出刀紀錄')
                  .setAuthor(message.author.username)
                  .addField('第一刀', one, true)
                  .addField('第二刀', two, true)
                  .addField('第三刀', scoreOne, true)
                  .addField('第三刀補償刀', scoreTwo, true)
                  .setTimestamp();
                message.channel.send(embed);
                break;
              }
              default: {
                message.channel.send('輸入有誤~~~');
              }
            }
          });

        // 填入Google Sheets 部屬後的URL
        if (score.indexOf('+') !== -1) {
          axios.post(URL, config);
          break;
        } else {
          axios.post(URL, config);
          break;
        }
      }

      case 'card': { // 抽卡? 抽點數
        const ran = Math.floor(Math.random() * 100) + 1;
        let ranResult;
        if (ran > 80) {
          ranResult = '運氣很好，抽卡GOGO';
        } else if (ran > 60) {
          ranResult = '運氣稍差，請燒香拜拜後再進行抽卡';
        } else {
          ranResult = '幫你上香 \\ |/';
        }
        message.channel.send(`${message.author.username}骰出了${ran}點，${ranResult}`);
        break;
      }

      case 'remind': {
        // 計算距離五點還有多久
        // 現在時間 小時/分/秒
        const now = DateTime.local().setZone('UTC+8');
        const newDay = DateTime.local().setZone('UTC+8').plus({ days: 1 });
        const newDayMonth = newDay.month;
        const nowMonth = now.month;
        const nowHour = now.hour;
        const nowMin = now.minute;
        const nowSec = now.second;
        // 凌晨5點結算，用29小時算秒數(因現在時間總秒數包含凌晨5點以前，故+5hr)
        const total = 29 * 60 * 60;
        // 現在時間的總秒數
        const nowTotal = nowHour * 60 * 60 + nowMin * 60 + nowSec;
        // 相差總秒數
        let dif = total - nowTotal;
        // 距離5點還有的時分秒
        let nextTime;

        if (nowMonth === newDayMonth) {
          if (nowHour < 5) {
            dif += 86400;
            nextTime = secondFormat(dif);
          } else {
            nextTime = secondFormat(dif);
          }
        } else {
          nextTime = secondFormat(dif);
        }


        // 填入Google Sheets 部屬後的URL
        // URL後面請放GET參數 q=remind
        await axios.get(URL)
          .then((res) => {
            message.channel.send(`距離${nowMonth === newDayMonth ? '5點' : '晚上12點'}${nextTime}，還有${res.data}刀`);
          });
        break;
      }

      default: {
        message.channel.send('沒有這個關鍵字喔~ 請重新輸入');
      }
    }
  }
});
