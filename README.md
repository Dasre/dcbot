# 公主連結 Discord填刀機器人

## 目前版本資訊：v2.1.0


---
v1.0.0版本使用的是Discord官方的discord.io套件

但因為該套件已一段時間未更新

在v2.0.0版本全面改用discord.js套件

---

## 2.1.0版更新
* 1.使用luxon.js套件解決在REPL.IT和Uptime Robot，UTC+8問題。(感謝ptt gisjhang大大提供解法)
* 2.戰對戰最後一天為2359結束。(感謝ptt gisjhang大大提出問題)
  * 如戰隊戰結束日期不為每月最後一天，須在做code調整。

## 安裝方法：

* 1.自行安裝好Node.js。
* 2.Clone下來，切換到資料夾，npm install需求套件。
* 3.在auth.json請將token改成自己的token，[教學連結](https://home.gamer.com.tw/creationDetail.php?sn=4433179)。
* 4.在v2版本內有四處URL需自行更換成Google Sheets部屬後的URL。
* 5.Google Sheets資料處理為透過Google Apps Script，請自行撰寫code與工作表對應，如有問題請與作者聯繫。
* ~~6.如欲使用REPL.IT和Uptime Robot，在計算"現在時間的總秒數"，請把hour+8。原因是`const nowTime = new Date();`這行會抓到UTC+0的時間。若有更好方法抓到UTC+8的時間，煩請Issues告知。~~


### 目前僅提供填刀、抽卡和查詢餘額刀數功能

* 填刀
  * 指令：!sc [第幾刀] [分數] [幾階段王]
  * 分數部分如有補償刀，請用"+"分隔主刀與補償刀傷害(ex: 15000+3000)
  * ``` JavaScirpt
    const config = JSON.stringify({
      num: num,
      date: '23',
      name: 'coe',
      one: scoreOne,
      one_other : scoreTwo,
      boss_one: boss,
    });
    ```
    此為將傳入Google Sheets內容，目前作者尚未與自己戰隊隊長討論Google Sheets表格設計，故目前date和name傳入的資料是寫死的。但已做好參數命名與取值。

* 抽卡
  * 指令：!card
  * random number，大於80為運氣好；61-80為運氣稍差；小於等於60燒香拜拜。

* 餘額刀數
  * 指令：!remind
  * 查詢到5點之前還有多久時間與剩餘刀數，剩餘刀數為透過Google Sheets取得，請自行撰寫Google Sheets doGet function。

### 預計v2.2.0版本將處理
* 填刀註記是否使用閃退機會
* 填刀的date與name更改
* ...

2020/03/04 Klshine

